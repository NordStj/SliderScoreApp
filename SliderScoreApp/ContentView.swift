//
//  ContentView.swift
//  SliderScoreApp
//
//  Created by Дмитрий on 12.10.2023.
//

import SwiftUI

struct ContentView: View {

    @State var targetValue = Int.random(in: 1...100)
    @State var currentValue = Double.random(in: 1...100)
    
    @State var showAlert = false
    
    var body: some View {
        
        VStack {
            VStack{
                Text("Подвиньте слайдер, как можно ближе к: \(targetValue)")
                HStack {
                    Text("0")
                    UiSlider(currentValue: $currentValue, alpha: computeScore())
                    Text("100")
                }
                .padding()
            }
            
            Button("Проверь меня!") {showAlert.toggle()}
                .padding()
                .alert("Your Score", isPresented: $showAlert, actions: {}) {
                    Text(computeScore().formatted())
                }
            Button("Начать заново") {updateValue()}
        }
        .padding()
    }
}

extension ContentView {
    
    private func updateValue() {
        targetValue = Int.random(in: 1...100)
        currentValue =  Double.random(in: 1...100)
    }
    
    private func computeScore() -> Int {
        let difference = abs(targetValue - lround(currentValue))
        return 100 - difference
    }
}

#Preview {
    ContentView()
}

