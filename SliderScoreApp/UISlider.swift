//
//  UISlider.swift
//  SliderScoreApp
//
//  Created by Дмитрий on 16.10.2023.
//

import SwiftUI

struct UiSlider: UIViewRepresentable {
    
    @Binding var currentValue: Double
    
    let alpha: Int
    
    func makeUIView(context: Context) -> UISlider {
        let uiSlider = UISlider()
        uiSlider.value = Float(currentValue)
        uiSlider.minimumValue = 0
        uiSlider.maximumValue = 100
        uiSlider.thumbTintColor = .red
        uiSlider.addTarget(
            context.coordinator,
            action: #selector(Coordinator.didEnd),
            for: .valueChanged
        )
        return uiSlider
    }
    
    func updateUIView(_ uiView: UISlider, context: Context) {
        uiView.thumbTintColor = .red.withAlphaComponent(CGFloat(alpha) / 100)
    }
    
    func makeCoordinator() -> Coordinator {
        Coordinator(currentValue: $currentValue)
    }
    
}

extension UiSlider {
    class Coordinator: NSObject{
        @Binding var currentValue: Double
        
        init(currentValue: Binding<Double>) {
            self._currentValue = currentValue
        }
        
        @objc func didEnd(_ sender: UISlider) {
            currentValue = Double(sender.value)
        }
    }
}

struct UISlider_Prewiews: PreviewProvider{
    static var previews: some View {
        UiSlider(currentValue: .constant(50.0), alpha: 100)
    }
}
