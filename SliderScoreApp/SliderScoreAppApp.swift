//
//  SliderScoreAppApp.swift
//  SliderScoreApp
//
//  Created by Дмитрий on 12.10.2023.
//

import SwiftUI

@main
struct SliderScoreAppApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
